package PrinterTest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.testng.annotations.Test;

import Printer.InReader;
import Printer.Printer1;

public class Printer1Test {
	
	@Test
	public void testCreatePhrase(){
		
		InReader  inreadermock = mock(InReader.class);
		
		ArrayList<String> mockresponse = new ArrayList<String>(); 		
		mockresponse.add("Hi, my name is Kostas");
		when(inreadermock.read()).thenReturn(mockresponse);
		
		
		
		Printer1 check = new Printer1();
		check.createPhrase();
		String strcheck = check.getToPrint(); 
		
		assert(strcheck.equals("Hi, my name is Kostas")) ;
		
	}
	

}
