package Printer;

import java.util.*;

public class Printer1 {

	private String toPrint="";

	public Printer1(){}

	public void createPhrase( ){
		ArrayList<String> myPhrases = new ArrayList<String>();
		myPhrases = new InReader().read();
        Iterator<String> myit = myPhrases.iterator();
		String output ="";
		while (myit.hasNext()){
			output = output+myit.next()+" ";
		}		
		this.toPrint = output.trim();
	}

	public void print(String phrase){
		System.out.println(phrase);
	}

	public String getToPrint() {
		return toPrint;
	}

	public void setToPrint(String toPrint) {
		this.toPrint = toPrint;
	}

}
